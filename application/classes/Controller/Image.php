<?php

class Controller_Image extends Controller{

	public function action_index()
	{
		$images = ORM::factory('Image')->find_all();
		$view = View::factory('image/index')->bind('images',$images);
		$this->response->body($view);
	}

	public function action_reload(){

		$images = ORM::factory('Image')->find_all();
 
		?>

		 
		<table>
			
			<?php if(count($images)) :?>
			<tr>
				<th>Title</th><th>Thumbnail</th><th>Date Added</th>
			</tr>
			<?php foreach($images as $image):?>
				<tr id=image-<?php echo $image->id;?> onclick="load_edit_form(<?php echo $image->id;?>);">
					<td><?php echo $image->title; ?></td>
					<td><img src="<?php echo url::base() . '/uploads/thumbs/' . $image->filename; ?>" alt="<?php echo $image->title?>"></td>
					<td><?php echo $image->date_added; ?></td>
				</tr>
			<?php endforeach; ?>
			<?php else:?>
				<p>No images found.</p>
			<?php endif; ?>
		</table>
	 

		<?php

	}

	public function action_delete(){
		
		$id = $this->request->param('id');

		if(empty($id) && !is_numeric($id)){
			return;
		}
			
		$image = ORM::factory('Image')->where('id','=',$id)->find();

		$image->delete();

	}

	public function action_edit(){
		$error_message = NULL;
        $filename = NULL;
        $error = NULL;

		$id = $this->request->param('id');

		if(empty($id) && !is_numeric($id)){
			return;
		}
			
		$image = ORM::factory('Image')->where('id','=',$id)->find();

        if ($this->request->method() == Request::POST)
        {

            try{

            	$image = ORM::factory('Image',$id);

            	//if old title changed, check for duplicates
            	if( $_POST['old_title'] != $_POST['title'])
        		{

            		$image->title = filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING);

            	}

            	 
            	if (isset($_FILES['image'])){

            		if($_FILES['image']['error'] == UPLOAD_ERR_NO_FILE){

            			// no file selected or image was not changed

            		}else{

            			$filename = $this->_save_image($_FILES['image']);

            			 if ( ! $filename )
				        {
				            $error_message = 'There was a problem while uploading the image.
				                Make sure it is uploaded and must be JPG/PNG/GIF file.';
				        }

	            		$image->filename = $filename;

            		}

 

	            } 
	            	
	            $image->save();

	            echo json_encode(['filename' => $image->filename]);

            }catch(ORM_Validation_Exception $e){

            	$errors = $e->errors('model');

            }

	        if(!empty($errors)){
	        	foreach($errors as $error){
	        		$error_message .= $error . '<br/>';
	        	}
	        	$response = ['error' => $error_message];

	        	echo json_encode($response);
	        }
	 	
	 		

        }else{

			$result = ['id' => $image->id,'title' => $image->title,'filename' => $image->filename,'date_added' => $image->date_added];

			echo json_encode($result);

        }

        
	}


	public function action_new()
    {
         
        $error_message = NULL;
        $filename = NULL;
        $error = NULL;
 
        if ($this->request->method() == Request::POST)
        {
            if (isset($_FILES['image']))
            {
            	try{
            		$filename = $this->_save_image($_FILES['image']);
	                $image = ORM::factory('Image');
	                $image->title = filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING);
	                $image->filename = $filename;
	                $image->save();

	               	echo json_encode(['filename' => $image->filename]);

            	}catch(ORM_Validation_Exception $e){
            		$errors = $e->errors('model');
            	}
            
            }

            if ( ! $filename)
	        {
	            $error_message = 'There was a problem while uploading the image.
	                Make sure it is uploaded and must be JPG/PNG/GIF file.';
	        }


	        if(!empty($errors)){
	        	foreach($errors as $error){
	        		$error_message .= $error . '<br/>';
	        	}

	        	$response = ['error' => $error_message];

	        	echo json_encode($response);
	        }
	 
	       
        }else{

        	 echo '<img src="' . url::base() . 'uploads/thumbs/photo.jpg" width="200px" height="200px" class="upload-preview" />';

        }
 


    }

    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }

        // echo 'APPPATH: ' . APPPATH . '<br/>';
        // echo 'SYSPATH: ' . SYSPATH . '<br/>';
        // echo 'DOCROOT: ' . DOCROOT . '<br/>';
 
        $directory = str_replace('\\', '/',DOCROOT) .'kohana-crud/uploads/'; // fix file separator error

 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 20)) . '.' . pathinfo($file,PATHINFO_EXTENSION);
 
            Image::factory($file)
                ->save($directory.$filename);

            Image::factory($file)
            	->resize(200,200,Image::AUTO)
                ->save($directory.'thumbs/'.$filename);
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }

}  