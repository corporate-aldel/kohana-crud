<?php
class Model_Image extends ORM{

	public function rules(){
		return array(
			'title' => array(
					array('not_empty'),
					// array('Model_Image::unique_title')
					array(array($this, 'unique'), array('title', ':value')),
				)
			);

	}

	// public static function unique_title($title)
	// {
	//     // Check if the title already exists in the database
	//     return ! DB::select(array(DB::expr('COUNT(title)'), 'total'))
	//         ->from('images')
	//         ->where('title', '=', $title)
	//         ->execute()
	//         ->get('total');
	// }

}