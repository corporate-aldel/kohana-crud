<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>Kohana Image CRUD v1.0</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

	<script type="text/javascript" src="<?php echo url::base();?>js/fancybox/jquery.easing-1.3.pack.js"></script>
	
	<script type="text/javascript" src="<?php echo url::base();?>js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>

	<script type="text/javascript" src="<?php echo url::base();?>js/fancybox/jquery.fancybox-1.3.4.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo url::base();?>js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<style>
body{
	background: #eee;
	font-family: 'Roboto', sans-serif;
}
.main-content {
    width: 800px;
    margin: 30px auto;
    padding: 30px;
    background: #fff;
}
.buttons-group {
	display: flex;
}
#add_new_image{
    text-decoration: none;
    background: #52ce6c;
    padding: 5px 10px;
    text-align: center;
    border: none;
    color: #fff;
    float: right;
    width: 150px;
    margin-bottom: 30px;
}
.form-action{
	text-decoration: none;
    background: #52ce6c;
    padding: 5px 10px;
    text-align: center;
    border: none;
    color: #fff;
    flex: 1;
    margin: 3px;
}
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}

tr:hover{
	background-color:#ffff99;
	cursor: pointer;
}

#targetOuter{	
	position:relative;
    text-align: center;
    background-color: #F0E8E0;
    margin: 20px auto;
    width: 200px;
    height: 200px;
	border-radius: 4px;
}
.btnSubmit {
    background-color: #565656;
    border-radius: 4px;
    padding: 10px;
    border: #333 1px solid;
    color: #FFFFFF;
    width: 200px;
	cursor:pointer;
}
.inputFile {
	padding: 5px 0px;
    /* margin-top: 8px; */
    background-color: #FFFFFF;
    width: 48px;
    overflow: hidden;
    opacity: 0;
    cursor: pointer;
    width: 100%;
    height: 100%;
}
.icon-choose-image {
	position: absolute;
    opacity: 0.1;
    top: 0;
    left: 0;
    /* margin-top: -24px; */
    /* margin-left: -24px; */
    width: 200px;
    height: 200px;
}
.upload-preview {border-radius:4px;}
	#body-overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: absolute;left: 0;top: 0;width: 100%;height: 100%;display: none;}
	#body-overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;
}
img.upload-preview {
    object-fit: contain;
}
</style>

</head>

<body>
<div class="main-content">
	<h2>Kohana Image Manager v1.0</h2>
	<a href="javascript:void(0)" onclick="load_add_form()" id="add_new_image">Add New</a>
	<div id="image-table">
		<table>
			
			<?php if(count($images)) :?>
			<tr>
				<th>Title</th><th>Thumbnail</th><th>Date Added</th>
			</tr>
			<?php foreach($images as $image):?>
				<tr id=image-<?php echo $image->id;?> onclick="load_edit_form(<?php echo $image->id;?>);">
					<td><?php echo $image->title; ?></td>
					<td><img src="<?php echo url::base() . '/uploads/thumbs/' . $image->filename; ?>" alt="<?php echo $image->title?>"></td>
					<td><?php echo $image->date_added; ?></td>
				</tr>
			<?php endforeach; ?>
			<?php else:?>
				<p>No images found.</p>
			<?php endif; ?>
		</table>
	</div>
</div>

<div id="form-container" style="display: none;">
	<?php if(isset($error_message)): ?>
	<div class="errors">	
		<?php echo $error_message; ?>	
	</div>
	<?php endif; ?>
	<div class="bgColor">
	<h2 id="form-title">Edit Image</h2>
	<div id="messages"></div>
	<form action="" id="uploadForm" enctype="multipart/form-data">
		<label for="title">Title</label>
		<input type="hidden" name="image_id" value="">
		<input type="hidden" name="old_title" value="">
		<input type="text" name="title">
		<div id="targetOuter">
			<div id="targetLayer">
				<img src="<?php echo url::base();?>uploads/thumbs/default.jpg" width="200px" height="200px" class="upload-preview">
			</div>
			
			<div class="icon-choose-image" >
				<input name="image" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
			</div>
		</div>
		<div class="buttons-group">
			<a id="load-big-img" class="form-action" href="javascript:void(0)">View Image</a>
			<a id="delete-link" class="form-action" href="javascript:void(0)" onclick="select_file()">Select File</a>
			<input type="submit" class="form-action" value="Save">
			<a id="delete-link" class="form-action" href="javascript:void(0)" onclick="delete_image()">Delete</a>
		</div>
	</form>

</div>


    <!-- jQuery Modal -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
	
	<script>
 
		function select_file(){
			$('#userImage').trigger('click');
		}


		// load add new form modal
		function load_add_form(){
				$('#uploadForm').attr('action','<?php echo url::base();?>image/new/');
				$('#form-title').html('Add New Image');
				$('input[name=image_id]').val('');
				$('input[name=title]').val('');
				$('input[name=old_title]').val('');
				$("#targetLayer").html('<img src="<?php echo url::base();?>uploads/thumbs/default.jpg" width="200px" height="200px" class="upload-preview" />');

				$('#form-container').modal('toggle');
		}
		

		// load edit form modal
		function load_edit_form(id){
			var id,title,filename,date_added;

			$.getJSON("<?php echo url::base();?>image/edit/" + id,function(data){

				console.log("value of id: " + data['id']);

				$('#messages').html('');

				$('#uploadForm').attr('action','<?php echo url::base();?>image/edit/' + data['id']);

				$('#form-title').html('Editing: <strong>' + data['title'] + '</strong>');

				$('input[name=image_id]').val(data['id']);



				$('input[name=title]').val(data['title']);

				$('input[name=old_title]').val(data['title']);

				$("#targetLayer").html('<img src="<?php echo url::base();?>uploads/thumbs/' + data['filename'] + '" width="200px" height="200px" class="upload-preview" />');

				$('#load-big-img').attr('href','<?php echo url::base();?>uploads/' + data['filename']);

				$('#load-big-img').attr('title', data['title']);

				$('#form-container').modal('toggle');
			});
		}

		function delete_image(){
			if(confirm('Are you sure you want to delete this image?')){
				
				var id = $('input[name=image_id]').val();

				$.ajax({
			        url: "<?php echo url::base();?>image/delete/" + id,
			        type: 'GET',
			        dataType: 'html', // added data type
			        success: function(res) {
			           reload();
			        }
			    });

				
			}
			return false;
		}

		//repaint container
		function reload(){

			$.get('<?php echo url::base();?>image/reload/',function(data){
				$('.close-modal').trigger('click');
				$('#image-table').html(data);
			});

		}

		 
	</script>

	<script type="text/javascript">
		function showPreview(objFileInput) {
		    if (objFileInput.files[0]) {
		        var fileReader = new FileReader();
		        fileReader.onload = function (e) {
		            $("#targetLayer").html('<img src="'+e.target.result+'" width="200px" height="200px" class="upload-preview" />');
					$("#targetLayer").css('opacity','0.7');
					$(".icon-choose-image").css('opacity','0.5');
		        }
				fileReader.readAsDataURL(objFileInput.files[0]);
		    }


		}

		$(document).ready(function (e) {
			$("#uploadForm").on('submit',(function(e) {
				e.preventDefault();
				$.ajax({
		        	url: $(this).attr('action'),
					type: "POST",
					data:  new FormData(this),
					dataType: 'json',
					beforeSend: function(){$("#body-overlay").show();},
					contentType: false,
		    	    processData:false,
					success: function(data)
				    {

				    	if (!("error" in data)) {
				    		
				    		$("#targetLayer").html('<img src="<?php echo url::base();?>uploads/thumbs/' +  data.filename + '>');

							$("#targetLayer").css('opacity','1');

							reload();
				    		
				    	}else{
				    		 
				    		$('#messages').html(data.error);
				    		
				    	}
						
					},
				  	error: function() 
			    	{

			    	} 	        
			   });
			}));


			$("#load-big-img").click(function(e) {

				e.preventDefault();

				$.fancybox({
					//'orig'			: $(this),
					'padding'		: 0,
					'href'			: $(this).attr('href'),
					'title'   		: $(this).attr('title'),
					'transitionIn'	: 'elastic',
					'transitionOut'	: 'elastic'
				});
			});
		});


	</script>
 
</body>

</html>



