<!doctype html>
<html class="no-js" lang="en">

<head>
    <title><?php echo (isset($title) ? $title : 'Untitled'); ?></title>
    <meta name="description" content="<?php echo (isset($description) ? $description : ' No description '); ?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
</head>

<body>
    <?php echo (isset($content)?$content:''); ?>



    <!-- jQuery Modal -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />


	<script type="text/javascript">
		function showPreview(objFileInput) {
		    if (objFileInput.files[0]) {
		        var fileReader = new FileReader();
		        fileReader.onload = function (e) {
		            $("#targetLayer").html('<img src="'+e.target.result+'" width="200px" height="200px" class="upload-preview" />');
					$("#targetLayer").css('opacity','0.7');
					$(".icon-choose-image").css('opacity','0.5');
		        }
				fileReader.readAsDataURL(objFileInput.files[0]);
		    }
		}

		$(document).ready(function (e) {
			$("#uploadForm").on('submit',(function(e) {
				e.preventDefault();
				$.ajax({
		        	url: $(this).attr('action'),
					type: "POST",
					data:  new FormData(this),
					beforeSend: function(){$("#body-overlay").show();},
					contentType: false,
		    	    processData:false,
					success: function(data)
				    {
						$("#targetLayer").html(data);
						$("#targetLayer").css('opacity','1');
						setInterval(function() {$("#body-overlay").hide(); },500);
					},
				  	error: function() 
			    	{

			    	} 	        
			   });
			}));
		});
</script>
</body>

</html>